var expect = require("chai").expect;
var {JSDOM} = require("jsdom");


describe("convertor testing", function () {
    var jsdom;
    before(async function () {
        jsdom = await
        JSDOM.fromFile("index.html", {
            resources: "usable",
            runScripts: "dangerously"
        });
        await
        new Promise(resolve =>
        jsdom.window.addEventListener("load", resolve)
    );
    });

    it('should return number betwen 1 7 if first param is 1, second is 7', function () {

        jsdom.window.document.getElementById("buttonGenerate").disabled = false;
        jsdom.window.document.getElementById("inputFrom").value = 1;
        jsdom.window.document.getElementById("inputTo").value = 7;
        jsdom.window.document.getElementById("buttonGenerate").click();
        expect(Number(jsdom.window.document.getElementById("inputResult").value)).to.be.above(0);
        expect(Number(jsdom.window.document.getElementById("inputResult").value)).to.be.below(8);
    });

    it('should return Wrong range if first param is 7, second is 1', function () {

        jsdom.window.document.getElementById("buttonGenerate").disabled = false;
        jsdom.window.document.getElementById("inputFrom").value = 7;
        jsdom.window.document.getElementById("inputFrom").onkeyup;
        jsdom.window.document.getElementById("inputTo").value = 1;
        jsdom.window.document.getElementById("inputTo").onkeyup;
        jsdom.window.document.getElementById("buttonGenerate").click();
        expect(jsdom.window.document.getElementById("inputResult").value).to.equal("Wrong range");
    });

    it('should return 0 if first param is 0, second is 0 and generate button pressed 3 times', function () {


        jsdom.window.document.getElementById("buttonGenerate").disabled = false;
        jsdom.window.document.getElementById("inputFrom").value = 1;
        jsdom.window.document.getElementById("inputTo").value = 2;
        jsdom.window.document.getElementById("buttonGenerate").click();
        jsdom.window.document.getElementById("buttonGenerate").click();
        jsdom.window.document.getElementById("buttonGenerate").click();
        expect(jsdom.window.document.getElementById("inputResult").value).to.equal("No more variants");
    });

    it('should return 0 if first param is 0, second is 0 and generate button pressed 3 times', function () {
        jsdom.window.document.getElementById("buttonGenerate").disabled = false;
        jsdom.window.document.getElementById("inputFrom").value = 0;
        jsdom.window.document.getElementById("inputTo").value = 0;
        jsdom.window.document.getElementById("buttonGenerate").click();
        expect(jsdom.window.document.getElementById("inputResult").value).to.equal("No more variants");
    });
});
