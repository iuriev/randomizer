window.onload = function () {
    document.getElementById("buttonGenerate").disabled = true;
};

var generate = document.getElementById("buttonGenerate");
var from = document.getElementById("inputFrom");
var to = document.getElementById("inputTo");
var result = document.getElementById("inputResult");
var reset = document.getElementById("buttonReset");

generate.addEventListener('click',getRandom);
from.addEventListener('keyup',checkParams);
to.addEventListener('keyup',checkParams);
reset.addEventListener('click', resetFields);
var winners = [];

function getRandom() {
    var min = Number(from.value);
    var max = Number(to.value) + 1;

    if (max < min) {
        result.value = "Wrong range";
        generate.disabled = true;
        return;
    }
    if (winners.length === (max - min)) {
        result.value = "No more variants";
        generate.disabled = true;
        return;
    }
    var randomInteger = generateRandomInt(min, max);

    if (checkElementInArray(randomInteger, winners)) {
        getRandom();
    } else {
        winners.push(randomInteger);
        result.value = randomInteger;
    }
}

function generateRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function checkElementInArray(value, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === value) {
            return true;
        }
    }
    return false;
}

function resetFields() {
    winners = [];
    from.value = "";
    to.value = "";
    result.value = "";
    generate.disabled = true;
}

function checkParams() {
    if (from.value === "" || to.value === "") {
        generate.disabled = true;
    } else {
        generate.disabled = false;
    }
}